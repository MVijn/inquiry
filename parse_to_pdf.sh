#!/bin/bash


## on ubuntu install markdown 
# apt install npm
# npm install markdown-pdf -g
# npm install markdown -g

for file in `ls | grep .md | sed -e 's/\.md//g'`
do	
	echo $file
	rm  ${file}.html
	md2html -f $file.md  -o ${file}.html
        markdown-pdf $file.md
done

